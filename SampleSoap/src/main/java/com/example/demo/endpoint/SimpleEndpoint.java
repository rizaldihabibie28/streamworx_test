package com.example.demo.endpoint;

import com.example.demo.request.Request;
import com.example.demo.request.RequestDetail;
import com.example.demo.response.RestResponse;
import com.example.demo.service.RestService;
import com.oracle.external.services.sampleservice.response.v1.Sampleservicerq;
import com.oracle.external.services.sampleservice.response.v1.Sampleservicers;

import lombok.RequiredArgsConstructor;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import org.springframework.ws.soap.SoapHeaderElement;
import org.springframework.ws.soap.server.endpoint.annotation.SoapHeader;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.transform.dom.DOMSource;
import java.util.Objects;

@Endpoint
@RequiredArgsConstructor
public class SimpleEndpoint {

    private static final String NAMESPACE_URI = "http://www.oracle.com/external/services/sampleservice/response/v1.0";
    private static final String NAMESPACE_URI_CREDENTIAL = "http://www.oracle.com";

    private final RestService restService;

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "sampleservicerq")
    @ResponsePayload
    public Sampleservicers sampleservice(
            @RequestPayload Sampleservicerq sampleservicerq,
            @SoapHeader("{"+NAMESPACE_URI_CREDENTIAL+"}authenticationheader") SoapHeaderElement soapHeaderElement) throws Exception {
        Sampleservicers simpleResponse1 = new Sampleservicers();

        if(validate(soapHeaderElement)) {

            simpleResponse1.setErrorCode("0000");
            simpleResponse1.setErrorMessage("Success");
            Request request = new Request();
            request.setRequestDetail(new RequestDetail(sampleservicerq));
            RestResponse response = restService.postToRest(request);
            simpleResponse1.setTrxId(response.getSampleServicers().getTrxId());

        } else {

            simpleResponse1.setErrorCode("1000");
            simpleResponse1.setErrorMessage("Permission Denied");
            simpleResponse1.setTrxId(sampleservicerq.getTrxId());

        }
        return simpleResponse1;

    }

    private boolean validate(SoapHeaderElement soapHeaderElement) {
        if(Objects.nonNull(soapHeaderElement)) {
            Element authenticationHeaderDomElement = (Element) ((DOMSource) soapHeaderElement.getSource()).getNode();
            NodeList usernameList = authenticationHeaderDomElement.getElementsByTagNameNS("http://www.oracle.com", "username");
            NodeList passwordList = authenticationHeaderDomElement.getElementsByTagNameNS("http://www.oracle.com", "password");

            if (Objects.nonNull(usernameList) && Objects.nonNull(passwordList)) {

                String username = usernameList.item(0).getTextContent();
                String password = passwordList.item(0).getTextContent();

                if(Objects.nonNull(username) && Objects.nonNull(password)) {
                    return true;
                }

            }
        }

        return false;

    }
}
