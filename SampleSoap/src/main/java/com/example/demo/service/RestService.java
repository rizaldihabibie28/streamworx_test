package com.example.demo.service;

import com.example.demo.interfaces.Posts;
import com.example.demo.request.Request;
import com.example.demo.response.RestResponse;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

@Service
@RequiredArgsConstructor
@Slf4j
public class RestService {

    @Qualifier("retrofitPostJson")
    private final Retrofit retrofitPostJson;

    public RestResponse postToRest(Request request) throws Exception {
        Posts posts = retrofitPostJson.create(Posts.class);
        Call<RestResponse> postRestResponseCall = posts.post(request);
        try {

            Response<RestResponse> response= postRestResponseCall.execute();
            if(response.code() != HttpStatus.OK.value()){
                throw new Exception("Unexpected error occurred");
            }
            return (RestResponse) response.body();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error occurred when calling post service",e);
            throw new Exception("Unexpected error occurred");
        }
    }

}
