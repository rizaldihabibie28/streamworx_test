package com.example.demo.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RestResponse {

    @SerializedName("sampleservicers")
    private ResponseDetail sampleServicers;

}
