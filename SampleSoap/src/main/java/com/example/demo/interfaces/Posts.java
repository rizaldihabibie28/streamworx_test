package com.example.demo.interfaces;

import com.example.demo.request.Request;
import com.example.demo.response.RestResponse;
import retrofit2.Call;
import retrofit2.http.*;

public interface Posts {

    @POST("/external/services/rest/sample-service")
    Call<RestResponse> post(@Body Request request);

}
