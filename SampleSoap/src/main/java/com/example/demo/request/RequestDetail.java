package com.example.demo.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.oracle.external.services.sampleservice.response.v1.Sampleservicerq;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RequestDetail {

    @JsonProperty(value = "service_id")
    private String serviceId;
    @JsonProperty(value = "order_type")
    private String orderType;
    private String type;
    @JsonProperty(value = "trx_id")
    private String trxId;

    public RequestDetail(Sampleservicerq sampleservicerq) {
        this.serviceId = sampleservicerq.getServiceId();
        this.orderType = sampleservicerq.getOrderType();
        this.type = sampleservicerq.getType();
        this.trxId = sampleservicerq.getTrxId();
    }

    public RequestDetail() {

    }
}
