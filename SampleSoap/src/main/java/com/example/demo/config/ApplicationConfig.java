package com.example.demo.config;

import com.google.gson.GsonBuilder;
import lombok.extern.slf4j.Slf4j;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.concurrent.TimeUnit;

@Configuration
@Slf4j
public class ApplicationConfig {

    @Value("${rest.base.url}")
    private String restBaseUrl;

    @Bean
    public Retrofit retrofitPostJson(){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor(string -> log.debug(string));
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        com.google.gson.Gson gson = new GsonBuilder()
                .create();
        return new Retrofit.Builder()
                .baseUrl(restBaseUrl)
                .client(new OkHttpClient.Builder().addInterceptor(interceptor).connectTimeout(60, TimeUnit.SECONDS).build())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

    }

}
