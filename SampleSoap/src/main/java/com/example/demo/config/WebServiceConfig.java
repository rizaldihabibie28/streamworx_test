package com.example.demo.config;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

@EnableWs
@Configuration
public class WebServiceConfig {



    @Bean
    public ServletRegistrationBean<MessageDispatcherServlet>
    messageDispatcherServletBean(ApplicationContext applicationContext) {
        MessageDispatcherServlet messageDispatcherServlet = new MessageDispatcherServlet();
        messageDispatcherServlet.setApplicationContext(applicationContext);
        messageDispatcherServlet.setTransformSchemaLocations(true);
        return new ServletRegistrationBean<>(messageDispatcherServlet, "/ws/*");

    }

    @Bean(name="simpleResponse")
    public DefaultWsdl11Definition defaultWsdl11Definition(XsdSchema simpleSchema) {
        DefaultWsdl11Definition defaultWsdl11Definition = new DefaultWsdl11Definition();
        defaultWsdl11Definition.setPortTypeName("SimplePort");
        defaultWsdl11Definition.setLocationUri("/ws");
        defaultWsdl11Definition.setResponseSuffix("rs");
        defaultWsdl11Definition.setRequestSuffix("rq");
        defaultWsdl11Definition.setTargetNamespace("http://www.oracle.com/external/services/sampleservice/response/v1.0");
        defaultWsdl11Definition.setSchema(simpleSchema);
        return defaultWsdl11Definition;
    }

    @Bean
    public XsdSchema simpleSchema() {
        return new SimpleXsdSchema(new ClassPathResource("simple-response.xsd"));
    }
}
