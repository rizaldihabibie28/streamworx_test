package com.example.demo.endpoint;

import com.example.demo.request.Request;
import com.example.demo.response.ResponseDetail;
import com.example.demo.response.RestResponse;
import com.example.demo.service.RestService;
import com.oracle.external.services.sampleservice.response.v1.Sampleservicerq;
import com.oracle.external.services.sampleservice.response.v1.Sampleservicers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.ws.soap.SoapHeaderElement;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.transform.dom.DOMSource;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class SimpleEndpointTest {

    @InjectMocks
    private SimpleEndpoint simpleEndpoint;

    @Mock
    private RestService restService;


    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void sampleserviceShouldReturnSuccess() throws Exception {

        Sampleservicerq sampleservicerq = new Sampleservicerq();
        SoapHeaderElement soapHeaderElement = mock(SoapHeaderElement.class);
        DOMSource domSource = mock(DOMSource.class);
        Element element = mock(Element.class);
        NodeList usernameMock = mock(NodeList.class);
        NodeList passwordMock = mock(NodeList.class);
        when(soapHeaderElement.getSource()).thenReturn(domSource);
        when(domSource.getNode()).thenReturn(element);
        when(element.getElementsByTagNameNS("http://www.oracle.com", "username")).thenReturn(usernameMock);
        when(element.getElementsByTagNameNS("http://www.oracle.com", "password")).thenReturn(passwordMock);
        Node usernameNode = mock(Node.class);
        Node passwordNode = mock(Node.class);
        when(usernameMock.item(0)).thenReturn(usernameNode);
        when(passwordMock.item(0)).thenReturn(passwordNode);
        when(usernameNode.getTextContent()).thenReturn("username");
        when(passwordNode.getTextContent()).thenReturn("password");
        RestResponse response = new RestResponse();
        ResponseDetail responseDetail = new ResponseDetail();
        responseDetail.setTrxId("sdsds-34567-sdaf67");
        response.setSampleServicers(responseDetail);
        when(restService.postToRest(any(Request.class))).thenReturn(response);
        sampleservicerq.setTrxId("trx_id987654");
        Sampleservicers sampleservicers = simpleEndpoint.sampleservice(sampleservicerq, soapHeaderElement);
        assertEquals("sdsds-34567-sdaf67", sampleservicers.getTrxId());
        assertEquals("0000", sampleservicers.getErrorCode());
        assertEquals("Success", sampleservicers.getErrorMessage());

    }

    @Test
    void sampleserviceShouldReturnPermissionDenied() throws Exception {

        Sampleservicerq sampleservicerq = new Sampleservicerq();
        SoapHeaderElement soapHeaderElement = mock(SoapHeaderElement.class);
        DOMSource domSource = mock(DOMSource.class);
        Element element = mock(Element.class);
        when(soapHeaderElement.getSource()).thenReturn(domSource);
        when(domSource.getNode()).thenReturn(element);
        when(element.getElementsByTagNameNS("http://www.oracle.com", "username")).thenReturn(null);
        sampleservicerq.setTrxId("trx_id4567");
        Sampleservicers sampleservicers = simpleEndpoint.sampleservice(sampleservicerq, soapHeaderElement);
        assertEquals("trx_id4567", sampleservicers.getTrxId());
        assertEquals("1000", sampleservicers.getErrorCode());
        assertEquals("Permission Denied", sampleservicers.getErrorMessage());

    }
}