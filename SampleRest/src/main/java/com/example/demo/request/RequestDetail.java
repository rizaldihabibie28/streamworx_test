package com.example.demo.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RequestDetail {

    @JsonProperty(value = "service_id")
    private String serviceId;
    @JsonProperty(value = "order_type")
    private String orderType;
    private String type;
    @JsonProperty(value = "trx_id")
    private String trxId;
}
