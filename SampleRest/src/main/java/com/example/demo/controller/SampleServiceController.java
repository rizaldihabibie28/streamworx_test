package com.example.demo.controller;

import com.example.demo.request.Request;
import com.example.demo.response.Response;
import com.example.demo.response.ResponseDetail;
import com.example.demo.utility.TransactionId;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/external/services/rest/sample-service")
public class SampleServiceController {

    @PostMapping
    public ResponseEntity<Response> postSomething(@RequestBody Request request) {

        Response response = new Response();
        ResponseDetail responseDetail = new ResponseDetail();
        responseDetail.setErrorCode("0000");
        responseDetail.setErrorMessage("Success");
        responseDetail.setTrxId(TransactionId.generate());
        response.setSampleServicers(responseDetail);

        return new ResponseEntity<>(response, HttpStatus.OK);

    }

}
