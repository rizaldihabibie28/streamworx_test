package com.example.demo.utility;

import java.util.UUID;

public class TransactionId {

    public static String generate() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }
}
