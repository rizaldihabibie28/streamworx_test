package com.example.demo.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Response {

    @JsonProperty(value = "sampleservicers")
    private ResponseDetail sampleServicers;

}
