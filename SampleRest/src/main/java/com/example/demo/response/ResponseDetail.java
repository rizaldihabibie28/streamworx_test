package com.example.demo.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseDetail {

    @JsonProperty(value = "error_code")
    private String errorCode;
    @JsonProperty(value = "error_message")
    private String errorMessage;

    @JsonProperty(value = "trx_id")
    private String trxId;

}
