package com.example.demo.controller;

import com.example.demo.request.Request;
import com.example.demo.request.RequestDetail;
import com.example.demo.response.Response;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.*;

class SampleServiceControllerTest {

    @InjectMocks
    private SampleServiceController sampleServiceController;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void shouldReturnSuccess() {
        Request request = new Request();
        RequestDetail requestDetail = new RequestDetail();
        requestDetail.setServiceId("23432424");
        requestDetail.setOrderType("ODT");
        requestDetail.setType("PO");
        requestDetail.setTrxId("654356789");
        request.setSampleServicerq(requestDetail);
        ResponseEntity<Response> responseResponseEntity = sampleServiceController.postSomething(request);
        Response actual = responseResponseEntity.getBody();
        assertEquals("0000", actual.getSampleServicers().getErrorCode());
        assertEquals("Success", actual.getSampleServicers().getErrorMessage());
        assertNotNull(actual.getSampleServicers().getTrxId());
    }

}